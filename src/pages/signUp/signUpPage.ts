import { Component } from '@angular/core';
import { baseUtils } from '../../utils/utils';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { auth } from '../../services/auth';
import { locationsList } from "../locations/locationsList"

@Component({
 	templateUrl: 'signUpPage.html'
})


export class signUpPage extends baseUtils {
 
  private isLoggedIn:boolean = false;
  constructor(private auth: auth, public alertCtrl:AlertController, public loadingCtrl:LoadingController ,public navCtrl:NavController) {
  	super()
  }
  cardTypes =  ["visa", "mastercard", "discover", "amex"];
  signupParams:any = {id:null, user:{id:null}, paymentInfo:{}}
  signUpSubmit(form){
  	if(form.valid==true)
  	{
      let loader = this.loadingCtrl.create({
          content: "loading..."
        })
      loader.present()
  		this.auth.signup(this.signupParams).then(data => {
        loader.dismiss()
        if(data.success==true)
          this.isLoggedIn = true;
        data.success!=true ? this.alertCtrl.create({subTitle:data.message}).present() : this.navCtrl.canGoBack() ? this.navCtrl.pop() : this.navCtrl.setRoot(locationsList) }
        )
  	}
  }
}