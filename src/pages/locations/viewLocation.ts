import { Component, ElementRef,ChangeDetectorRef ,ChangeDetectionStrategy } from '@angular/core';
import { NavController, ModalController, ActionSheetController, NavParams,AlertController, MenuController } from 'ionic-angular';
import { restApi } from '../../services/restApi';
import { baseUtils } from '../../utils/utils';
import { mapApi } from '../../services/mapApi';
import { gpsLocation } from '../../services/gpsLocation';
import { Diagnostic } from '@ionic-native/diagnostic';

declare var cordova;

@Component({
  selector:'map-points',
  changeDetection:ChangeDetectionStrategy.Default,
  templateUrl: './viewLocation.html'
})
export class viewLocationPage {
  
  public _location:any;
  public _locationPoint:any[] = [];
  public _google;
  private _markers:any = {};
  private map;
  public point = null;
  public clickedPoint = null;
  private polygon = null;
  public mapHeight:string;
  public apiPromise:Promise<any>;
  public pointTypes:Array<any>  = [];
  public autoPlay = true;
  public selPointType = null;
  public currentPoint = null;
  public watchID = null;
  public userPos:{lat:number, lng:number} = null;
  public userCircle =  null;
  public userPoint =  null;
  public centerOn = "location";


  constructor(public diagnostic:Diagnostic,  public actionSheet:ActionSheetController, public modalCtlr:ModalController ,public navCtrl:NavController,public gpsLocation:gpsLocation, public menuCtrl:MenuController,public alertCtrl:AlertController,public ref: ChangeDetectorRef, public utils:baseUtils, public mapApi: mapApi, public element:ElementRef ,public navParams:NavParams ,public restApi:restApi) {
    this.apiPromise = this.mapApi.ready()
    this.setHeight()
    this.restApi.getPointTypes().then(o=> this.pointTypes=o);
    window.addEventListener("resize",()=>{
        this.setHeight()
        this.ref.detectChanges()
    })
    window.addEventListener("orientationchange",()=>{
        this.setHeight()
        this.ref.detectChanges()
    })


  }

  ionViewWillEnter(){
    this._location = this.navParams.data["location"];
    this._locationPoint = this.navParams.data["locationPoint"];
    this.apiPromise.then(_goo => this.initMap(_goo))
  }

  ngAfterViewInit()
  {
    this.menuCtrl.swipeEnable(false);
    setTimeout(()=>this.menuCtrl.open('right'),1500);
  }


  positionChange(lat, lng)
  {
    let newPos = this.userPos
    this.userPos = this.userPos || {lat:0,lng:0};
    this.userPos.lat = parseFloat(lat);
    this.userPos.lng = parseFloat(lng);

    //if(!newPos)
    //  this.centerOnCurrentPos(false)
    

    console.log("User position is ")
    console.log(this.userPos)
    let currentPos = new this._google.maps.LatLng(this.userPos.lat, this.userPos.lng);

    let point = this._locationPoint["find"](o=>{
      let p = new this._google.maps.LatLng(parseFloat(o["lat"]), parseFloat(o["lng"]))
      return this.pointInCircle(p, currentPos ,parseFloat(o["radius"]))
    })

    console.log("Found point as ")
    console.log(point)

    if(!point && !this.currentPoint)
    {
      
    }
    else if(point && this.currentPoint && point["id"]==this.currentPoint["id"])
    {
      
    }
    else if(!point && this.currentPoint)
    {
      this.onPointerLeave()
    }
    else if(point && !this.currentPoint)
    {
      this.onPointEnter(point)
    }

    

     this.userPoint = this.userPoint || new this._google.maps.Marker({
          map: this.map,
          draggable: false,
          icon:{url:'assets/icon/blue-dot.png'}
        });


    this.userCircle = this.userCircle || new this._google.maps.Circle({
          map: this.map,
          radius: 10,
          fillColor: '#2222AA',
          strokeColor: '#000000',
          strokeOpacity: 0.3,
          strokeWeight: 1,
        });

    this.userCircle.bindTo('center', this.userPoint, 'position');
    this.userPoint.setPosition(currentPos)
    
    console.log("userPoint is ")
    console.log(this.userPoint)

  }

  pointInCircle(center, point, radius)
  {
      return (this._google.maps.geometry.spherical.computeDistanceBetween(point, center) <= radius)
  }

  setHeight()
  {
    this.mapHeight = window.innerHeight*0.8 + 'px';
  }
  initMap(_google)
  {
    var me = this;
    this._google = _google;
    this._location["polygon"] = this._location["polygon"] || {id:null};
    this._location["polygon"]["points"] = this._location["polygon"]["points"] || [];
    this._location["polygon"]["points"] = this._location["polygon"]["points"].map(o=>{
        o["radius"] = parseFloat(o["radius"]);
        o["lat"] = parseFloat(o["lat"]);
        o["types"] = o["types"] || []
        o["lng"] = parseFloat(o["lng"]);
        return o;
      })
    this._location["lat"] = parseFloat(this._location["lat"]) || -34;
    this._location["lng"] = parseFloat(this._location["lng"]) || 150;
    this._location["zoom"] = parseFloat(this._location["zoom"]) || 8;

    this.map = this.getMap()
    
    if(this.hasPolygon()==false)
    {
      this.noBoundsWarning()
      return
    } 
    else if(!(this._locationPoint.length > 0))
    {
      this.noPointsWarning()
      return
    }
    else
    {
      this.polygon = this.getPolygon()
      this.polygon.setMap(this.map)
      this.addMarkers();

    }

    this.setUpLocationWatch()
    this.centerMap(false)

    
  }


  isLocationEnabled():Promise<boolean> {
    if("undefined" == typeof cordova)
      return Promise.resolve(true)
    return this.diagnostic.isLocationEnabled()
    .then(t=> t!=true ? false : this.diagnostic.getLocationAuthorizationStatus())
    .then((status)=>{
      return status=='GRANTED' || status=='authorized';
    }).catch(e=>{
      console.log(e)
      return false;
    })
  }

  setUpLocationWatch()
  {

      
     
     this.checkLocationSettings()
     .then(f=>{
          if(!this.gpsLocation.hasStarted())
             this.gpsLocation.start()
          this.gpsLocation.positionEvent.subscribe(o=>{
            console.log(o);
            this.positionChange(o.coords.latitude, o.coords.longitude);
          })

          if(this.gpsLocation.position)
            {
              this.positionChange(this.gpsLocation.position.coords.latitude, this.gpsLocation.position.coords.longitude);
              //this.centerOnCurrentPos(false)
            }
     })

      
  }


  checkLocationSettings()
  {
    return this.isLocationEnabled()
    .then(y=>{
      if(y!=true)
      {
        let modal = this.modalCtlr.create('LocationPermissionPage',null,{enableBackdropDismiss:false})
        modal.present()
        return new Promise((res,rej)=>{
          modal.onWillDismiss(res)
        })
      }
      else
        return true
    })
  }

  centerOnCurrentPos(closeMenu)
  {
    if(!this.map)
      return

    if(this.userPos)
    {
        this.map.setCenter(this.userPos);
        this.map.setZoom(19)
    }
    if(closeMenu==true)
      {
        this.menuCtrl.close('right')
      }
  }


  centerOnLocation(closeMenu)
  {

    if(!this.map)
      return

    this.map.fitBounds(this.polygon.getBounds());
    if(closeMenu==true)
     {
        this.menuCtrl.close('right')
     }
  }


  centerMap(v)
  {
    
    this.centerOn=="location" ? this.centerOnLocation(v) : this.centerOnCurrentPos(v)
  }




  getMap()
  {
    return new this._google.maps.Map(document.getElementById('maptag-points'), {
          center: {lat: this._location["lat"], lng: this._location["lng"]},
          zoom: this._location["zoom"]
        });
  }

  noBoundsWarning()
  {
      this.alertCtrl.create({message:"This location doesnt have any bounds yet"}).present()
  }
  noPointsWarning()
  {
    this.alertCtrl.create({message:"This location doesnt have any points yet"}).present()
  }
  pointClick(marker)
  {
    var index  = marker.id;

    let point = this._locationPoint[index];

    if(! point || !point.audioFile)
    {
      let alertCtlr = this.alertCtrl.create({subTitle:"No audio file here"})
      alertCtlr.present()
      return
    }


    this.clickedPoint = point;
    console.log(point)


    this.actionSheet.config.set('mode','md')
    let actionSheet = this.actionSheet.create({enableBackdropDismiss:false,title:`Playing audio for ${point.name}`, buttons:[
       {
         text: 'Cancel',
         role: 'cancel',
         icon: 'md-close'
       }
      ]})

    actionSheet.present()
    actionSheet.onWillDismiss(()=>{
      this.clickedPoint = null;
      point  = null;
    })
    this.ref.detectChanges()


  }

  filterPoints()
  {
    if(this.selPointType=='null' || this.selPointType==null)
    {
      for(var i in this._markers)
        this.showMarker(this._markers[i])
    }
    else
    {
       for(var i in this._markers)
        {
          if(this._locationPoint[i].pointtypes.find(o=>o["id"]==this.selPointType.id))
            this.showMarker(this._markers[i])
          else
            this.hideMarker(this._markers[i])

        }
    }


  }

  showMarker(m){
    m.setMap(this.map)
    m.circle.setMap(this.map)
  }
  hideMarker(m){
    m.setMap(null)
    m.circle.setMap(null)

  }

  getPoint()
  {
    return this.point
  }

  onPointEnter(point){
    this.currentPoint  = point;

  }

  hasPointType(point)
  {
    if(point==null)
      return false

    else if(this.selPointType==null || this.selPointType=='null')
      return true

    return point.pointtypes.find(o=>o["id"]==this.selPointType.id)!=undefined
  }

  onPointerLeave(){
    this.currentPoint = null;
  }



  //construct polygon from location's polygon
  getPolygon()
  {
    return new this._google.maps.Polygon({
          paths: this._location["polygon"]["points"]
        })
  }

  addMarkers()
  {
     if(this.hasPolygon()==false)
      {
          this.noBoundsWarning()
         return 
      }
    var me = this;
    this._locationPoint.forEach((o:any,index)=>{
        var marker = new me._google.maps.Marker({
          position:{lat: parseFloat(o["lat"]), lng:parseFloat(o["lng"])},
          map: me.map,
          draggable: false,
          title: o["name"]
        });

        marker.addListener('click', function()
        {
           me.pointClick(marker)
        });

        // Add circle overlay and bind to marker
        var circle = new me._google.maps.Circle({
          map: me.map,
          radius: parseFloat(o["radius"]),
          fillColor: '#AA0000'
        });

        circle.bindTo('center', marker, 'position');
        marker.circle = circle;
        marker.id = index;
        me._markers[index] = marker;
    })
  }


  hasPolygon():boolean
  {
    return this._location["polygon"]["points"].length > 2;
  }

}
