import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { locationsList } from './locationsList';


@NgModule({
  declarations: [
    locationsList,
  ],
  imports: [
  	IonicPageModule.forChild(locationsList),
  ],
  exports: [
    locationsList
  ]
})
export class locationsModule {}
