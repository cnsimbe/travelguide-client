import { Component } from '@angular/core';
import { NavController, AlertController, IonicPage, LoadingController } from 'ionic-angular';
import { locationPage } from '../locations/locationPage';
import { restApi } from '../../services/restApi';
import { auth } from  '../../services/auth';

@IonicPage()
@Component({
  templateUrl: 'locationsList.html'
})
export class locationsList {
  

  public appName:string = 'CIT EEZ';
  public locations:any[] = null;
  public userLocations:any[] = null;
  public locationPage:any = locationPage;
  constructor(public loadingCtrl:LoadingController, public alertCtrl:AlertController,public navCtrl:NavController, public auth:auth, public restApi:restApi) {
    this.appName = "CIT EEZ"
  }

  ionViewWillEnter(){
    let prms = [];
    prms.push(this.restApi.getLocations(null));
    if(this.auth.user)
      prms.push(this.restApi.userLocations(null))

    let loader = this.loadingCtrl.create({
          content: "loading..."
        })
        loader.present()
  	Promise.all(prms).then(values => {
      loader.dismiss()
      this.locations = values[0];

      if(values.length > 1)
        this.userLocations = values[1]
    }).catch(o=>{
      
      console.log(o)
        loader.dismiss()
      this.alertCtrl.create({subTitle:"There was a problem loading this page"}).present()
    })
  }


  goToLocation(location)
  {
    let userloc = this.userLocations ? this.userLocations["find"](o=>o.id==location.id) : null;
    this.navCtrl.push(this.locationPage, {location:location, userlocation:userloc})

  }

  isUserLocation(location)
  {
    return this.userLocations && this.userLocations["find"](o=>o.id==location.id)
  }


}
