import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, LoadingController } from 'ionic-angular';
import { locationPage } from '../locations/locationPage';
import { loginPage } from '../login/login';
import { restApi } from '../../services/restApi';
import { auth } from  '../../services/auth';

@Component({
  templateUrl: 'userLocationsList.html'
})
export class userLocationsList {
  

  public appName:string = 'CIT EEZ';
  public userLocations:any[] = null;
  public locationPage:any = locationPage;
  public loginPage = loginPage;
  constructor(public navCtrl:NavController, public menuCtrl:MenuController, public alertCtrl:AlertController, public loadingCtrl:LoadingController, public auth:auth, public restApi:restApi) {
    this.appName = "CIT EEZ"
  }

  ionViewWillEnter(){


    if(this.auth.user)
      {
        let loader = this.loadingCtrl.create({
          content: "loading..."
        })
        loader.present()
        this.restApi.userLocations(null).then(data=>{
          this.userLocations = data
          loader.dismiss()
        }).catch(o=>{
          loader.dismiss()
          this.alertCtrl.create({subTitle:"There was a problem loading this page"}).present()
        })
  	  }
    else
  		this.navCtrl.push(this.loginPage)
  }

  
  goToLocation(location)
  {
    let userloc = this.userLocations ? this.userLocations["find"](o=>o.id==location.id) : null;
    this.navCtrl.push(this.locationPage, {location:location, userlocation:userloc})

  }


}
