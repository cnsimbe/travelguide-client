import { Component } from '@angular/core';

import { baseUtils } from '../../utils/utils';
import { loginPage } from '../login/login';
import { locationsList } from '../locations/locationsList';
import { viewLocationPage } from '../locations/viewLocation';
import { signUpPage } from '../signUp/signUpPage';
import { auth } from  '../../services/auth';
import { restApi } from  '../../services/restApi';
import { NavController, MenuController, NavParams,AlertController,LoadingController } from 'ionic-angular';
@Component({
  templateUrl: './locationPage.html',
})
export class locationPage extends baseUtils {
	public loginPage = loginPage;
	public signUpPage = signUpPage;
	public location =  null;
	public userlocation = null;
	public viewLocationPage = viewLocationPage;
	constructor(public navCtrl:NavController, public menuCtrl:MenuController, public LoadingController:LoadingController, public restApi:restApi, public alertCtrl:AlertController, public auth:auth ,public navParams:NavParams){
		super()

	}

	ionViewWillEnter(){
    this.location = this.navParams.data["location"];
    this.userlocation = this.navParams.data["userlocation"];
    

    if(this.auth.user && !this.userlocation)
    {
    	let loader = this.LoadingController.create({
          content: "loading..."
        })
        loader.present()
        this.restApi.userLocations(null).then(data=>{
          this.userlocation = data.find(o=>o["id"]==this.location["id"])
          loader.dismiss()
        }).catch(o=>{
          loader.dismiss()
          this.alertCtrl.create({subTitle:"There was a problem loading this page"}).present()
        })
    }
  }

  goToUserLocation(loc){
  	var loader = this.LoadingController.create({
			      content: "Please wait..."
			    });
  	loader.present()
  	this.restApi.getLocationPoints({"id":loc["id"]}).then(o=>{
  		loader.dismiss()
  		this.navCtrl.push(this.viewLocationPage, {location:loc, locationPoint:o})
  	}).catch(()=>{
  		loader.dismiss()
  		this.alertCtrl.create({subTitle:"There was a problem loading this page"}).present()
  	})
  }

  buyConfirmation(){
  	let alert = this.alertCtrl.create({
	    title: 'Confirm purchase',
	    message: 'This location costs CAD' + this.location.price,
	    buttons: [
	      {
	        text: 'Cancel',
	        role: 'cancel'
	      },
	      {
	        text: 'Buy',
	        handler: () => {
	        	var loader = this.LoadingController.create({
			      content: "Please wait..."
			    });
			    loader.present()
	        	this.restApi.buyLocation(this.location).then(o=> {
	        			loader.dismiss();
	        			(o && o.success==true) ? this.userlocation=this.location : this.alertCtrl.create({title:"There a problem processing your payment"}).present()
	        		}).catch(o=>
	        		{
	        			this.alertCtrl.create({title:"There a problem processing your payment"}).present()
	        			loader.dismiss()
	        		})
	        }
	          	
	      }
	    ]
	  });
	  alert.present();
  }

  signInWarning(){
  	this.alertCtrl.create({title:"You are must be logged in to tour this location"}).present()
  }

}