import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocationPermissionPage } from './location-permission';

@NgModule({
  declarations: [
    LocationPermissionPage,
  ],
  imports: [
    IonicPageModule.forChild(LocationPermissionPage),
  ],
})
export class LocationPermissionPageModule {}
