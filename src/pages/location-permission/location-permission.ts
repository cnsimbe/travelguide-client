import { Component } from '@angular/core';
import { IonicPage, Platform, ViewController, NavController, NavParams } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
/**
 * Generated class for the LocationPermissionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-location-permission',
  templateUrl: 'location-permission.html',
})
export class LocationPermissionPage {
  
  isAuth:boolean =  false;
  constructor(private viewCtrl:ViewController,private backgroundGeo:BackgroundGeolocation,private diagnostic:Diagnostic,private platform:Platform) {
    
    this.diagnostic.registerLocationStateChangeHandler((status)=>{
      this.authMode()
    })
  }

  authMode() {
    this.diagnostic.getLocationAuthorizationStatus()
    .then((status)=>{
      console.log(status)
      this.isAuth = status=='GRANTED' || status=='authorized';
    })
    .then(a=>this.diagnostic.isLocationEnabled())
    .then(a=> (a==true && this.isAuth==true) ? this.viewCtrl.dismiss() : "")
  }

  ionViewWillEnter() {
    
    this.diagnostic.requestLocationAuthorization('always')
    .then(t=>{
      this.isAuth = t == 'GRANTED'|| status=='authorized'
      return this.diagnostic.isLocationEnabled()
    })
    .then(e=>{
      if(e!=true)
       this.backgroundGeo.showLocationSettings()
      else if(this.platform.is('android'))
       this.authMode()
    })
    .catch(error=>console.log(error))
    this.authMode()
    
  }

  enablePermission() {
    return new Promise((res,rej)=>{
      if(this.platform.is('android'))
      {
        this.diagnostic.requestLocationAuthorization('always')
        .then(t=>{
            this.isAuth = t == 'GRANTED'
            this.authMode()
            if(t=='DENIED_ALWAYS')
              this.backgroundGeo.showAppSettings()
        })
      }
      else if(this.platform.is('ios'))
        this.backgroundGeo.showAppSettings()  
    })
    
  }

  enableLocation() {

    this.enablePermission()
    this.diagnostic.isLocationEnabled()
    .then(a=>{
      if(a!=true)
        this.backgroundGeo.showLocationSettings()
    })
    
  }

  ionViewCanLeave():Promise<boolean> {
    return this.diagnostic.isLocationEnabled()
    .then(t=> t!=true ? false : this.diagnostic.getLocationAuthorizationStatus())
    .then((status)=>{
      this.isAuth = status=='GRANTED' || status=='authorized';
      return this.isAuth==true;
    })
  }
}
