import { Component } from '@angular/core';
import { NavController, NavParams, AlertController,LoadingController } from 'ionic-angular';
import { restApi } from '../../services/restApi';
import { baseUtils } from '../../utils/utils';
import { auth } from '../../services/auth';
@Component({
  templateUrl: 'userEditPage.html'
})
export class userEditPage {
  
  public user:any = null;
  
  constructor(public navCtrl:NavController, private auth:auth ,private alertCtrl: AlertController, private LoadingController:LoadingController,   private utils:baseUtils, private navParams:NavParams , private restApi:restApi) {}

  ionViewWillEnter()
  {
      this.user =this.navParams.data['user'] || {first_name:"New", is_staff:true, is_superuser:false ,last_name:"User", is_active:true}


  }

  warningRequired(title, message)
  {
    this.alertCtrl.create({title:title, subTitle:message}).present()
  }
  saveUser()
  {
    if(!this.user["first_name"])
    {
      this.warningRequired("Error","first name is required")
      return
    }


    if(!this.user["username"])
    {
      this.warningRequired("Error","username is required")
      return
    }

    if(!this.user["last_name"])
    {
      this.warningRequired("Error","last name is required")
      return
    }

    if(!this.user["email"])
    {
      this.warningRequired("Error","email is required")
      return
    }


    var me = this;
    var loader = this.LoadingController.create({
      content: "Please wait..."
    });
    loader.present()
    this.restApi.updateAdminUsers(this.user).then(user=>{
      loader.dismiss();me.navCtrl.pop()
    }).catch(o => loader.dismiss())
   
  }


}
