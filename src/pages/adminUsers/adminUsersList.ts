import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { restApi } from '../../services/restApi';
import { auth } from  '../../services/auth';
import { userEditPage }  from './userEditPage';

@Component({
  templateUrl: 'adminUsersList.html'
})
export class adminUsersList {
  

  public appName:string = 'CIT EEZ';
  public locations:any[] = [];
  public getAdminUsers:any[] = [];
  public HomePage:any = HomePage;
  public userEditPage:any = userEditPage;
  constructor(public navCtrl:NavController, public auth:auth, private restApi:restApi) {
    this.appName = "CIT EEZ"
  }

  ionViewWillEnter(){
  	this.restApi.getAdminUsers().then(data => this.getAdminUsers = data)
  }

  editUser(user)
  {
    this.navCtrl.push(this.userEditPage, {user:user})
  }



  updateUser(user)
  {
  	this.restApi.updateAdminUsers(user).then(data => user=data.data)
  }


}
