import { Component } from '@angular/core';
import { NavController, NavParams, AlertController,LoadingController } from 'ionic-angular';
import { restApi } from '../../services/restApi';
import { baseUtils } from '../../utils/utils';
import { auth } from '../../services/auth';
@Component({
  templateUrl: 'locationPage.html'
})
export class locationPage {
  
  public location:any = null;
  public selectedUser:any = null;
  public locationPoint:any[] = [];
  public adminusers:any[] = null;
  public draftlocation:Object ={};
  public draftlocationPoint:Object[] = [];
  public originalPointIds:Number[] = [];

  public editState:string = null;
  constructor(public navCtrl:NavController, public auth:auth ,public alertCtrl: AlertController, public LoadingController:LoadingController,   public utils:baseUtils, public navParams:NavParams , public restApi:restApi) {}

  ionViewWillEnter()
  {
      this.location = {};
      this.adminusers =this.navParams.data['adminusers']
      this.location = this.navParams.data['location'] || {name:'New Location', id:null};
      this.location["polygon"] = this.location["polygon"] || {"id":null};
      this.location["polygon"]["points"] = this.location["polygon"]["points"] || [];

      this.location["polygon"]["points"] = this.location["polygon"]["points"].map(o=>{
        o["lat"] = parseFloat(o["lat"]);
        o["lng"] = parseFloat(o["lng"]);
        return o;
      })

      this.location["lat"] = parseFloat(this.location["lat"]) || -34;
      this.location["lng"] = parseFloat(this.location["lng"]) || 150;
      this.location["zoom"] = parseFloat(this.location["zoom"]) || 8;
      this.location["price"] = parseFloat(this.location["price"]) || 0;

      this.locationPoint = this.navParams.data['locationPoint'] || [];

      this.locationPoint = this.locationPoint.map(o=>{
        o["id"] = parseFloat(o["id"]);
        o["radius"] = parseFloat(o["radius"]) || 10;
        o["lat"] = parseFloat(o["lat"]);
        o["lng"] = parseFloat(o["lng"]);
        this.originalPointIds.push(o["id"])
        return o;
      })

      this.navCtrl.canGoBack = this.canGoBack;


      if(this.adminusers && this.location.adminuser)
        this.selectedUser = this.adminusers["find"](o=> this.location.adminuser.id == o.id)


  }
  onSelectUser()
  {
    this.location.adminuser = this.selectedUser
  }
  canGoBack()
  {
    return this.editState==null;
  }

  editMapBounds()
  {
    this.draftlocation = this.utils.clone(this.location);
    this.draftlocationPoint = this.utils.clone(this.locationPoint);
    this.editState =  'bounds';
  }

  saveMapBounds()
  {
    this.location = this.utils.clone(this.draftlocation);
    this.locationPoint = this.utils.clone(this.draftlocationPoint);
    this.draftlocation = null;
    this.draftlocationPoint = null;
    this.editState =  null;
  }

  exitMapBounds()
  {

    this.editState =  null;
    this.draftlocation = null;
    this.draftlocationPoint = null;
  }

  editMapPoints()
  {
    this.draftlocation = this.utils.clone(this.location)
    this.draftlocationPoint = this.utils.clone(this.locationPoint)
    this.editState =  'points';
  }

  saveMapPoints()
  {
    this.location = this.utils.clone(this.draftlocation);
    this.locationPoint = this.utils.clone(this.draftlocationPoint);
    this.draftlocation = null;
    this.draftlocationPoint = null;
    this.editState =  null;
  }

  exitMapPoints()
  {
    this.editState =  null;
    this.draftlocation = null;
    this.draftlocationPoint = null;
  }

  warningRequired(title, message)
  {
    this.alertCtrl.create({title:title, subTitle:message}).present()
  }

   upoints()
    {
      return Promise.all(this.locationPoint.map(o => {
        return this.restApi.updateLocationPoint(o)
      }))
    }

  dpoints(toDelete:Number[])
  {
    return Promise.all(toDelete.map(o => {
      return this.restApi.deleteLocationPoint({id:o})
    }))
  }
  saveLocation()
  {
    this.location["price"] = parseFloat(this.location["price"])
    if(!this.location["name"])
    {
      this.warningRequired("Error","Location name is required")
      return
    }

    if(!this.location["description"])
    {
      this.warningRequired("Error","Location description is required")
      return
    }

    if(!this.location["price"] && this.location["price"] !=0)
    {
      this.warningRequired("Error","Location price is required")
      return
    }

    if(this.location["polygon"]["points"].length <=2)
    {
      this.warningRequired("Error","Location must have bounds")
      return
    }


    var me = this;
    var loader = this.LoadingController.create({
      content: "Please wait..."
    });
    loader.present()
    this.restApi.updateLocation(this.location).then(z=> {
      this.location = z.data;
      this.locationPoint = this.locationPoint.map(o => {
        o["location"] = z.data;
        return o;
      })
      var toDelete = this.originalPointIds.filter(o => this.locationPoint.findIndex(point => point["id"]==o)<0);

      if(toDelete.length > 0)
      {
        me.dpoints(toDelete).then(f => me.upoints().then(o => {loader.dismiss();me.navCtrl.pop()})).catch(o => loader.dismiss())
      }
      else
        me.upoints().then(o => {loader.dismiss();me.navCtrl.pop()}).catch(o => loader.dismiss())
      
      
    }).catch(o => loader.dismiss())
   
  }


}
