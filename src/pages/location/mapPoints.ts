import { Component, ElementRef,ChangeDetectorRef ,ChangeDetectionStrategy, Input } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { restApi } from '../../services/restApi';
import { baseUtils } from '../../utils/utils';
import { mapApi } from '../../services/mapApi';

@Component({
  selector:'map-points',
  changeDetection:ChangeDetectionStrategy.Default,
  templateUrl: 'mapPoints.html'
})
export class mapPointsPage {
  
  public _location:Object;
  public _locationPoint:Object[] = [];
  public _google;
  private _markers:Object = {};
  private map;
  public point = null;
  private polygon = null;
  public mapHeight:string;

  @Input()set locationPoint(l:any){
    this._locationPoint = l;
    
  }
  @Input()set location(l:any){
    this._location = l;
   this.mapApi.ready().then(_goo => this.initMap(_goo));
    
  }


  constructor(public navCtrl:NavController,public alertCtrl:AlertController,public ref: ChangeDetectorRef, public utils:baseUtils, public mapApi: mapApi, public element:ElementRef ,public navParams:NavParams ,public restApi:restApi) {
    this.setHeight()
    window.addEventListener("resize",()=>{
        this.setHeight()
        this.ref.detectChanges()
    })
    window.addEventListener("orientationchange",()=>{
        this.setHeight()
        this.ref.detectChanges()
    })


  }


  setHeight()
  {
    this.mapHeight = window.innerHeight*0.65 + 'px';
  }
  initMap(_google)
  {
    var me = this;
    this._google = _google;
    this._location["polygon"] = this._location["polygon"] || {id:null};
    this._location["polygon"]["points"] = this._location["polygon"]["points"] || [];
    this._location["polygon"]["points"] = this._location["polygon"]["points"].map(o=>{
        o["lat"] = parseFloat(o["lat"]);
        o["types"] = o["types"] || []
        o["lng"] = parseFloat(o["lng"]);
        return o;
      })
    this._location["lat"] = parseFloat(this._location["lat"]) || -34;
    this._location["lng"] = parseFloat(this._location["lng"]) || 150;
    this._location["zoom"] = parseFloat(this._location["zoom"]) || 8;

    this.map = this.getMap()

    this.map.addListener('center_changed', function() {
      me.updateLocation()
    });

    this.map.addListener('zoom_changed', function() {
      me.updateLocation()
    });
    
    if(this.hasPolygon()==false)
    {
      this.noBoundsWarning()
      return
    } 
    else
    {
      this.polygon = this.getPolygon()
      this.polygon.setMap(this.map)
      this.addMarkers();
    }
  }

  updateLocation()
  {
    this._location["zoom"] = this.map.getZoom();
    this._location["lat"] = this.map.getCenter().lat();
    this._location["lng"] = this.map.getCenter().lng();
  }




  getMap()
  {
    return new this._google.maps.Map(document.getElementById('maptag-points'), {
          center: {lat: this._location["lat"], lng: this._location["lng"]},
          zoom: this._location["zoom"]
        });
  }

  addPoint()
  {
     if(this.hasPolygon()==false)
      {
          this.noBoundsWarning()
         return 
      }
    var me = this;
    var point = this.map.getCenter().toJSON();
    point["id"] = null;
    point["name"] = "New Point";
    point["visibility"] = true
    point["radius"] = 10
    this._locationPoint.push(point);
    var index = this._locationPoint.length -1;
    var marker = new me._google.maps.Marker({
          position:point,
          map: me.map,
          draggable: true,
          animation: me._google.maps.Animation.DROP,
          title: "New Point"
        });

    marker.addListener('click', function()
    {
       me.pointClick(marker)
    });

    marker.addListener('drag', function()
    {
       me.pointDrag(marker)
    });

    // Add circle overlay and bind to marker
    var circle = new me._google.maps.Circle({
      map: me.map,
      radius: point["radius"],
      fillColor: '#AA0000'
    });

    circle.bindTo('center', marker, 'position');
    marker.circle = circle;

    marker.id = index;
    me._markers[index] = marker;

    this.pointClick(marker);
  }

  updateVisibility()
  {
    this.ref.detectChanges()
  }

  deletePoint()
  {
    if(this.hasPolygon()==false)
      {
          this.noBoundsWarning()
         return 
      }
    if(this.point)
    {
      var index = this._locationPoint.indexOf(this.point);
      this._markers[index].setMap(null);
      this._markers[index].circle.setMap(null);
      for(var i in this._markers)
      {
        this._markers[i].setMap(null)
        this._markers[i].circle.setMap(null);
        this._google.maps.event.clearListeners(this._markers[i], 'click')
        this._markers[i].circle = null
        this._markers[i] = null
      }
      this._markers = {}
      this.point = null;

      this._locationPoint.splice(index,1)
      this.addMarkers()
    }
    else
    {
      this.noPointSelected()
    }
    
  }

  noBoundsWarning()
  {
      this.alertCtrl.create({message:"This location doesnt have any bounds yet"}).present()
  }
  noPointSelected()
  {
      this.alertCtrl.create({message:"No Point Selected"}).present()
  }
  updatePoint(marker)
  {
    
    this.point["lat"] = marker.getPosition().lat()
    this.point["lng"] = marker.getPosition().lng()
    this.ref.detectChanges()
    setTimeout(()=>this.ref.detectChanges());
  }

  pointDrag(marker)
  {
    if(!this.point || this.point!= this._locationPoint[marker.id])
      this.pointClick(marker);

    this.updatePoint(marker)

  }

  pointClick(marker)
  {
    var index = null
    if(this.point)
      {
        index =  this._locationPoint.indexOf(this.point);
        if(index!=marker.id)
          this._markers[index].setAnimation(null);
      }

    index = marker.id;

    this.point = this._locationPoint[index];
    this._markers[index].setAnimation(this._google.maps.Animation.BOUNCE);

    

    this.ref.detectChanges()
    setTimeout(()=>this.ref.detectChanges());


  }

  getPoint()
  {
    return this.point
  }



  //construct polygon from location's polygon
  getPolygon()
  {
    return new this._google.maps.Polygon({
          paths: this._location["polygon"]["points"]
        })
  }

  addMarkers()
  {
     if(this.hasPolygon()==false)
      {
          this.noBoundsWarning()
         return 
      }
    var me = this;
    this._locationPoint.forEach((o,index)=>{
        var marker = new me._google.maps.Marker({
          position:{lat: parseFloat(o["lat"]), lng:parseFloat(o["lng"])},
          map: me.map,
          draggable: true,
          title: o["name"]
        });

        marker.addListener('click', function()
        {
           me.pointClick(marker)
        });

        marker.addListener('drag', function()
        {
           me.pointDrag(marker)
        });

        // Add circle overlay and bind to marker
        var circle = new me._google.maps.Circle({
          map: me.map,
          radius: o["radius"],
          fillColor: '#AA0000'
        });

        circle.bindTo('center', marker, 'position');
        marker.circle = circle;
        marker.id = index;
        me._markers[index] = marker;
    })
  }

  radiusChange()
  {
    var index =  this._locationPoint.indexOf(this.point);
    this.point.radius = parseFloat(this.point.radius)
    this._markers[index].circle.setRadius(parseFloat(this.point.radius))
  }

  hasPolygon():boolean
  {
    return this._location["polygon"]["points"].length > 2;
  }

  seeFile(point,file)
  {
    point["audioFile"] = file;
  }

}
