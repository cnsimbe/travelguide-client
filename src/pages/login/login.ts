import { Component } from '@angular/core';
import { baseUtils } from '../../utils/utils';
import { NavController, AlertController, LoadingController,ViewController } from 'ionic-angular';
import { auth } from '../../services/auth';
import { forgetPasswordPage } from '../forgetPassword/forgetPassword';
import { locationsList } from '../locations/locationsList';
import { landingPage } from '../landingPage/landingPage';
@Component({
 	templateUrl: 'login.html'
})


export class loginPage extends baseUtils {
 
  private isLoggedIn:boolean = false;
  constructor(private auth: auth, public viewCtrl:ViewController ,public alertCtrl:AlertController, public loadingCtrl: LoadingController, public navCtrl:NavController) {
  	super()
  }

   loginParams:any = {}
  _forgetPasswordPage:any = forgetPasswordPage;
  loginSubmit(form){
  	if(form.valid==true)
  	{
      let loader = this.loadingCtrl.create({
          content: "loading..."
        })
      loader.present()
  		this.auth.loginUser(this.loginParams).then(data => {
        loader.dismiss()
        if(data.success==true)
          this.isLoggedIn = true;
        data.success!=true ? this.alertCtrl.create({subTitle:"Invalid login Credentials"}).present() : this.navCtrl.canGoBack() ? (this.navCtrl.getPrevious(this.viewCtrl).component == landingPage ? this.navCtrl.setRoot(locationsList): this.navCtrl.pop()) : this.navCtrl.setRoot(locationsList) }
        )
  	}
  }
}