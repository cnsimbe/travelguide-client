import { Component } from '@angular/core';


import { loginPage } from '../login/login';
import { locationsList } from '../locations/locationsList';
import { signUpPage } from '../signUp/signUpPage';
import { restApi } from  '../../services/restApi';
import { NavController } from 'ionic-angular';
@Component({
  templateUrl: 'landingPage.html',
})
export class landingPage {
	public loginPage = loginPage;
	public signUpPage = signUpPage;
	public locationsList = locationsList;
	public landingPage = {};
	constructor(public navCtrl:NavController, public restApi: restApi){
		this.restApi.getLandingPage().then(o=>{
			this.landingPage = this.restApi.server + o.url
		})
	}

}