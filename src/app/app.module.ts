import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { travelApp } from './app.component';


import { baseUtils } from '../utils/utils';
import { auth } from '../services/auth';
import { restApi } from '../services/restApi';
import { mapApi } from '../services/mapApi';
import { gpsLocation } from '../services/gpsLocation';

import { forgetPasswordPage } from '../pages/forgetPassword/forgetPassword';
import { locationPage } from '../pages/locations/locationPage';
import { locationsList } from '../pages/locations/locationsList';
import { userLocationsList } from '../pages/locations/userLocationsList';
import { loginPage } from '../pages/login/login';
import { signUpPage } from '../pages/signUp/signUpPage';
import { landingPage } from '../pages/landingPage/landingPage';
import { viewLocationPage } from '../pages/locations/viewLocation';

import { Device } from "@ionic-native/device";
import { File } from "@ionic-native/file";
import { Keyboard } from '@ionic-native/keyboard';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';

@NgModule({
  declarations: [
    travelApp,
    locationPage,
    locationsList,
    userLocationsList,
    forgetPasswordPage,
    loginPage,
    signUpPage,
    landingPage,
    viewLocationPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(travelApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    travelApp,
    locationPage,
    locationsList,
    userLocationsList,
    forgetPasswordPage,
    loginPage,
    signUpPage,
    landingPage,
    viewLocationPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    StatusBar,
    SplashScreen,
    File,
    Device,
    Keyboard,
    BackgroundGeolocation,
    restApi,mapApi,auth,baseUtils,gpsLocation,
    Diagnostic
    ]
})
export class AppModule {}
