import { Component, ViewChild,AfterViewInit, OnInit } from '@angular/core';
import { Platform, MenuController, AlertController } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { loginPage } from '../pages/login/login';
import { locationsList } from '../pages/locations/locationsList';
import { landingPage } from '../pages/landingPage/landingPage';
import { signUpPage } from '../pages/signUp/signUpPage';
import { userLocationsList } from '../pages/locations/userLocationsList';
import { auth } from '../services/auth';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
declare var cordova;

@Component({
  templateUrl: 'app.html',
})
export class travelApp implements AfterViewInit{
  rootPage:any;
  @ViewChild('mainNav') navCtrl: NavController;

  constructor(private platform: Platform, private backgroundGeo:BackgroundGeolocation, private statusBar:StatusBar, private splash:SplashScreen,  public menuCtrl:MenuController, public auth:auth, public alertCtrl:AlertController) {
    platform.ready().then(() => {
    
    this.splash.hide()
    
    const config: BackgroundGeolocationConfig = {
              desiredAccuracy: 10,
              stationaryRadius: 20,
              distanceFilter: 30,
              startForeground:false,
              debug: false, //  enable this hear sounds for background-geolocation life-cycle.
              stopOnTerminate: true, // enable this to clear background location settings when the app terminates
      };

      if("undefined" != typeof cordova)
      {
        console.log("Background tracking started")
        this.backgroundGeo.configure(config)
        .subscribe(e=>{
          this.backgroundGeo.finish()
        })
        
        this.backgroundGeo.start()
      }
      
   
      
      this.auth.is_staff = false;
    });
  }

  ngAfterViewInit()
  {
      let pagePushed = false
      this.menuCtrl.swipeEnable(false);
  		this.auth.isLoggedIn(null).then(data => {

  		data.success==false ? this.navCtrl.push(landingPage) : this.navCtrl.push(locationsList)
      pagePushed = true
  	}).catch(o=>{
      if(!pagePushed)
        this.navCtrl.push(landingPage) 
      this.alertCtrl.create({subTitle:"There was a problem loading this page"}).present()
    })
  }

  openHome()
  {
    this.navCtrl.setRoot(locationsList)
  }

  logout()
  {
    this.auth.logoutUser().then(o=>{this.navCtrl.setRoot(landingPage)}).catch(o=>{
      this.auth.forceLogout()
      this.alertCtrl.create({subTitle:"Error"}).present()
    })
  }

   login()
  {
    this.navCtrl.push(loginPage)
  }


   signup()
  {
    this.navCtrl.push(signUpPage)
  }

  openUserLocations()
  {
    if(this.auth.user)
      this.navCtrl.setRoot(userLocationsList)
    else
      this.alertCtrl.create({title:"You are must be logged in to see this page"}).present()
  }

  openAllLocations() {
    this.navCtrl.setRoot(locationsList)
  }
}