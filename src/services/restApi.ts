import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import {baseUtils} from '../utils/utils';


@Injectable()
export class restApi extends baseUtils
{
	private jsonHeader = new Headers({'Content-Type': 'application/json'});
	private formHeader = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

	private jsonOptions = new RequestOptions({ headers: this.jsonHeader});
	private formOptions = new RequestOptions({ headers: this.formHeader});

	public server = "http://45.79.100.170:5000";
	private APP_AUTH_HEADER = "APP-AUTH-HEADER";

	private isLoggedInUrl = this.server + "/isLoggedIn";
	private loginUserUrl = this.server + "/loginUser";
	private logoutUserUrl = this.server + "/logoutUser";
	private changePasswordUrl = this.server + "/changePassword";
	private signupUrl = this.server + "/signup";

	private userProfileUrl = this.server + "/userProfile";
	private userLocationUrl = this.server + "/userLocations";
  private adminUsersUrl = this.server + "/adminUsers";

	private locationUrl = this.server + "/locations";
	private locationPointUrl = this.server + "/locationPoints";
	private locationPolygonUrl = this.server + "/locationPolygons";

  private paymentUrl = this.server + "/payment";
  private pointTypesUrl = this.server + "/pointTypes";

  private landingPageUrl = this.server + "/getLandingPage";


	constructor(private http: Http) { super() }

	getJsonOptions():RequestOptions {
		this.jsonOptions.headers.set(this.APP_AUTH_HEADER, localStorage.getItem(this.APP_AUTH_HEADER))
		return this.jsonOptions
	}

	getTextOptions():RequestOptions {
		this.jsonOptions.headers.set(this.APP_AUTH_HEADER, localStorage.getItem(this.APP_AUTH_HEADER))
		return this.jsonOptions
	}

	getFormOptions():RequestOptions {
		this.formOptions.headers.set(this.APP_AUTH_HEADER, localStorage.getItem(this.APP_AUTH_HEADER))
		return this.formOptions
		
	}



	  //auth
	  isLoggedIn(params): Promise<any> {
    return this.http.post(this.isLoggedInUrl, null, this.getJsonOptions())
               .toPromise()
               .then(response => response.json())
     }

    loginUser(params): Promise<any> {
    var me = this;
    return this.http.post(this.loginUserUrl, this.objectToUrl(params), this.getFormOptions())
               .toPromise()
               .then(function(response){
               		var res = response.json();
               		localStorage.setItem(me.APP_AUTH_HEADER, res[me.APP_AUTH_HEADER]);
               		return res
               })
  	}

  	logoutUser(): Promise<any> {
  	var me = this;
    return this.http.post(this.logoutUserUrl, null, this.getJsonOptions())
               .toPromise()
               .then(function(response){
               		localStorage.removeItem(me.APP_AUTH_HEADER);
               		return response.json();
               })
     }

     buyLocation(params):Promise<any>{
       return this.http.post(this.paymentUrl, JSON.stringify(params), this.getJsonOptions())
               .toPromise()
               .then(response => response.json() as any)
     }

  	changePassword(params): Promise<any> {
    return this.http.post(this.changePasswordUrl,this.objectToUrl(params), this.getFormOptions())
               .toPromise()
               .then(response => response.json())
  	}
	  //locations
	  signup(params): Promise<any> {
    return this.http.post(this.signupUrl, JSON.stringify(params), this.getJsonOptions())
               .toPromise()
               .then(response => response.json())
     }

    userLocations(params): Promise<any[]> {

    return this.http.get(this.userLocationUrl + '?' + this.objectToUrl(params), this.getJsonOptions())
               .toPromise()
               .then(response => response.json() as any[])
  	}

  	getUserProfile(): Promise<any> {
    return this.http.get(this.userProfileUrl, this.getJsonOptions())
               .toPromise()
               .then(response => response.json() as any)
  	}


    getLandingPage(): Promise<any> {
    return this.http.get(this.landingPageUrl, this.getFormOptions())
               .toPromise()
               .then(response => response.json() as any)
    }


    getPointTypes(): Promise<any> {
    return this.http.get(this.pointTypesUrl, this.getJsonOptions())
               .toPromise()
               .then(response => response.json() as any)
    }


  	updateUserProfile(params): Promise<any> {
    return this.http.post(this.userProfileUrl, JSON.stringify(params), this.getJsonOptions())
               .toPromise()
               .then(response => response.json() as any)
  	}


    getAdminUsers(): Promise<any> {
    return this.http.get(this.adminUsersUrl, this.getJsonOptions())
               .toPromise()
               .then(response => response.json() as any)
    }



    updateAdminUsers(params): Promise<any> {
    return this.http.post(this.adminUsersUrl, JSON.stringify(params), this.getJsonOptions())
               .toPromise()
               .then(response => response.json() as any)
    }


  	getLocations(params): Promise<any[]> {
    return this.http.get(this.locationUrl+ '?' + this.objectToUrl(params), this.getJsonOptions())
               .toPromise()
               .then(response => response.json() as any[])
  	}



  	updateLocation(params): Promise<any> {
    return this.http.post(this.locationUrl, JSON.stringify(params), this.getJsonOptions())
               .toPromise()
               .then(response => response.json() as any)
  	}
    
    deleteLocation(params): Promise<any> {
    return this.http.delete(this.locationUrl, new RequestOptions({body:JSON.stringify(params), headers: this.getJsonOptions().headers}))
               .toPromise()
               .then(response => response.json() as any)
    }

    getLocationPoints(params): Promise<any[]> {
    return this.http.get(this.locationPointUrl + '?' +this.objectToUrl(params) ,this.getJsonOptions())
               .toPromise()
               .then(response =>response.json().map(o=>{
                   if(o["audioFile"])
                      o["audioFile"].url = this.server + o["audioFile"].url + "&APP_AUTH_HEADER=" + this.getJsonOptions().headers.get("APP-AUTH-HEADER");
                    return o
               }))
  	}


  	//TODO Take care of file input
  	updateLocationPoint(params): Promise<any> {
    var me = this;
     if(!(params["audioFile"] instanceof File))
      return this.http.post(this.locationPointUrl, JSON.stringify(params), this.getJsonOptions())
               .toPromise()
               .then(response => response.json() as any)
     else
      return new Promise<any>((resolve, reject) => {
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();
            
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            var data = {}
            for(var i in params)
             {
              if(i!='audioFile')
                data[i] = params[i]
              else if(i=='audioFile' && params[i] instanceof File)
                formData.append(i, params[i])
              else
                data[i] = params[i]
             }

            formData.append("data", JSON.stringify(data))

            xhr.open("POST", this.locationPointUrl, true);
            var headers = me.getFormOptions().headers;

            headers.forEach((v,n) =>{
                  if(n.toLowerCase()!='content-type')
                    xhr.setRequestHeader(n, v[0]);
            })
            xhr.send(formData);
        });
  	}

  	deleteLocationPoint(params): Promise<any> {
    return this.http.delete(this.locationPointUrl, new RequestOptions({body:JSON.stringify(params), headers: this.getJsonOptions().headers}))
               .toPromise()
               .then(response => response.json() as any)
  	}


  	getLocationPolygons(params): Promise<any[]> {
    return this.http.get(this.locationPolygonUrl+ '?' + this.objectToUrl(params), this.getJsonOptions())
               .toPromise()
               .then(response => response.json() as any[])
  	}


  	updateLocationPolygon(params): Promise<any> {
     return this.http.post(this.locationPolygonUrl, JSON.stringify(params), this.getJsonOptions())
               .toPromise()
               .then(response => response.json() as any)
     
  	}

  	deleteLocationPolygon(params): Promise<any> {
    return this.http.delete(this.locationPolygonUrl, new RequestOptions({body:JSON.stringify(params), headers: this.getJsonOptions().headers}))
               .toPromise()
               .then(response => response.json() as any)
  	}

}