import { Injectable, EventEmitter } from '@angular/core';
import { AlertController } from 'ionic-angular';
import {baseUtils} from '../utils/utils';
@Injectable()
export class gpsLocation extends baseUtils
{
  public watchID:any = null;
  public positionEvent = new EventEmitter()
  public position = null;
  public isStart = false;
  constructor(public alertCtrl:AlertController) { 
    super()
  }
  start()
  {
    console.log("geostarting");
    let me = this;
    navigator.geolocation.clearWatch(me.watchID);
      console.log("Called set up geolocation")
      me.watchID = navigator.geolocation.watchPosition(function(position) {
          console.log("GeoLocation CallBack")
          console.log(position)
          me.position = position;
          me.positionEvent.emit(position)
      }, function(err) {
          if(err.code==err.PERMISSION_DENIED)
          {
              navigator.geolocation.clearWatch(me.watchID);
              console.log("Permission denied")
          }
          else if(err.code==err.POSITION_UNAVAILABLE)
          {
            console.log("Position unavailable")
          }
          else if(err.code==err.TIMEOUT)
          {
            console.log("Position timeout")
          }
      },{ enableHighAccuracy: true});
  }
  hasStarted()
  {
    return this.isStart
  }
	
}