import { Injectable } from '@angular/core';

import {baseUtils} from '../utils/utils';
import {restApi} from './restApi'

@Injectable()
export class auth extends baseUtils
{


	constructor(private restApi: restApi) { super() }

	public user:any = null;
  public is_staff:boolean = null;

	//auth
	isLoggedIn(params): Promise<any> {
        params = params || {}
        params["is_staff"] = this.is_staff;
    		return this.restApi.isLoggedIn(params).then(o => {
    			this.user = o["success"]==true ? o["user"] : null
          return o
    		})
     }

    loginUser(params): Promise<any> {
      params = params || {}
      params["is_staff"] = this.is_staff;
      return this.restApi.loginUser(params).then(o => {
          this.user = o["success"]==true ? o["user"] : null
          return o
        })
  	}

    signup(params): Promise<any> {
      params = params || {}
      params["is_staff"] = this.is_staff;
      return this.restApi.signup(params).then(o => {
          this.user = o["success"]==true ? o["tourist"]["user"] : null
          return o
        })
    }

  	logoutUser(): Promise<any> {
	  	return this.restApi.logoutUser().then(o => {
    			this.user = null
    			return o
    		})
     }

     forceLogout()
     {
       this.user = null
     }


     getUserProfile(): Promise<any> {
    	return this.restApi.getUserProfile()
  	}



  	updateUserProfile(params): Promise<any> {
	    return this.restApi.updateUserProfile(params).then(o => {
    			this.user = o["data"]["user"]
    			return o
    		})
  	}


    getAdminUsers(): Promise<any> {
    	return this.restApi.getAdminUsers()
    }



    updateAdminUsers(params): Promise<any> {
    	return this.restApi.updateAdminUsers(params).then(o => {
    			this.user = o["data"]
    			return o
    		})
    }



}