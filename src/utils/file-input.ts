import { Component, ElementRef,ChangeDetectorRef, EventEmitter , Input,Output } from '@angular/core';
import {  AlertController } from 'ionic-angular';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
  selector:'ng-file',
  templateUrl: 'file-input.html',
})

export class fileInput {
  
  public _file:any = null;
  public _mediaRecorder:any;
  public isRecording:boolean = false;
  public maxFileNameLength:string;
  private reader = new FileReader();
  @Input()set file(_f:Object){
    this._file = _f
  }
  @Input() name:string;
  @Output() ngModelChange = new EventEmitter();
  constructor(private element:ElementRef, private alertCtl:AlertController, private sanitizer:DomSanitizer ,private ref:ChangeDetectorRef) {

    this.setFileNameLength()
    window.addEventListener("resize",()=>{
        this.setFileNameLength()
        this.ref.detectChanges()
    })
  	window.addEventListener("orientationchange",()=>{
        this.setFileNameLength()
        this.ref.detectChanges()
    })
  }

  setFileNameLength()
  {
    this.maxFileNameLength = window.innerWidth*0.45 + 'px';
  }

  getFileElement()
  {
  	return this.element.nativeElement.querySelector("input[type=file]")
  }

  ngAfterViewInit()
  {
  	var me = this;
  	this.getFileElement().onchange = function(event)
  	{
      event.stopPropagation()
      event.preventDefault()
      event.stopImmediatePropagation()

  		var fileElement = me.getFileElement()
  		me._file = fileElement.files[0] as File || false;
      if(me._file)
      {
        me.reader.abort()
         me.reader.onload = function (e) {
           if(me._file)
             {
               me._file["url"] = me.sanitizer.bypassSecurityTrustResourceUrl(me.reader.result);
               me._file["content_type"] = me._file.type;
               me.ngModelChange.emit(me._file);
               me.ref.detectChanges();
             }
             else
               window.URL.revokeObjectURL(me._file);
             
         }
         me.reader.readAsDataURL(me._file)
      }
      else
        {
          me.ngModelChange.emit(me._file);
          me.ref.detectChanges();
        }
  	}
  }

  removeFile(event){
    this.reader.abort();
  	event.stopPropagation()
  	event.preventDefault()
  	var element = this.getFileElement()
  	this._file = false;
  	element.value =  null;
  	this.ngModelChange.emit(this._file);
  }


  startRecording(event)
  {
    var me = this;
    event.stopPropagation()
    event.preventDefault()
    navigator["getUserMedia"] = (navigator["getUserMedia"] ||
                          navigator["mozGetUserMedia"] ||
                          navigator["msGetUserMedia"] ||
                          navigator["webkitGetUserMedia"]);


    if (navigator["getUserMedia"]) {
      var constraints = { audio: true };
      var chunks = [];
      var onSuccess = function(stream) {
        me._mediaRecorder = new window["MediaRecorder"](stream);

        me._mediaRecorder.onstop = function(e) {
          if(me.isRecording==false)
          {
            chunks = []
            return;
          }
          me.isRecording = false;
          var blob = new Blob(chunks, { 'type' : 'audio/ogg; codecs=opus' });
          chunks = [];
          me._file = new File([blob], "new recording.ogg", {type:blob.type});
          me._file["content_type"] = me._file["type"]
          me._file ["url"] = me.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
          me.ngModelChange.emit(me._file);
          var element = me.getFileElement()
          element.value =  null;
          me.ref.detectChanges();
          setTimeout(()=>me.ref.detectChanges(),1)
       }

        me._mediaRecorder.ondataavailable = function(e) {
          chunks.push(e.data);
        }
        me._mediaRecorder.start()
        me.isRecording = true;
        me.ref.detectChanges();
        setTimeout(()=>me.ref.detectChanges(),1)
      }

      var onError = function(err) {
        me.errorOpeningRecordingDevice();
        me.ref.detectChanges();
        setTimeout(()=>me.ref.detectChanges(),1)
      }

      navigator["getUserMedia"](constraints, onSuccess, onError);
    } else {
       me.audioRecordingNotSupported()
    }
  }

  saveRecording(event)
  {
    event.stopPropagation()
    event.preventDefault()
    if(this._mediaRecorder && this._mediaRecorder.state=='recording')
    {
        this._mediaRecorder.stop();
        this._mediaRecorder = null;
    }
  }


  deleteRecording(event)
  {
    event.stopPropagation()
    event.preventDefault()
    if(this._mediaRecorder && this._mediaRecorder.state=='recording')
    {
        this.isRecording = false
        this._mediaRecorder.stop();
        this._mediaRecorder = null;
    }
  }

  audioRecordingNotSupported()
  {
    this.alertCtl.create({title:"Audio Recording Not Supported"}).present()
  }

  errorOpeningRecordingDevice()
  {  
    this.alertCtl.create({title:"Error Opening Recording Device"}).present()
  }



}
